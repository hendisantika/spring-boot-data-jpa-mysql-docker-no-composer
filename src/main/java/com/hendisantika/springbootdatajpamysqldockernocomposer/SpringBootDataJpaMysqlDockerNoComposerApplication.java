package com.hendisantika.springbootdatajpamysqldockernocomposer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataJpaMysqlDockerNoComposerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDataJpaMysqlDockerNoComposerApplication.class, args);
    }

}

