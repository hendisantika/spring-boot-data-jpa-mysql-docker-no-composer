FROM amazoncorretto:21-alpine-jdk
MAINTAINER Hendi Santika <hendisantika@yahoo.co.id>
VOLUME /tmp
EXPOSE 8080
ADD target/spring-boot-data-jpa-mysql-docker-no-composer-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]