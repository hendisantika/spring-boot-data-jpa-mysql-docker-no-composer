# spring-boot-data-jpa-mysql-docker-no-composer

Spring Boot and Spring Data JPA (MySQL) REST Api example with docker (without docker-compose)

There are two main ways that can be used to build and run applications with docker.

1. Using docker-compose for managing  and running dependencies (servers) and link them
2. Manually managing and running the dependencies and link them (without docker-compose)

In this repository we will be focusing on the second approach. That is “without docker-compose approach”


#### Things to do :

1. `git clone git@gitlab.com:hendisantika/spring-boot-data-jpa-mysql-docker-no-composer.git`
2. Create a docker container for MySQL.
    
    MySQL Team has provided an official docker image of MySQL through 
    Docker Hub.  (https://hub.docker.com/_/mysql/) . Therefore we can create 
    the MysQL docker container executing the following command.  
    It will first check the local docker registry for finding the requested mysql image. 
    If it is not available, it will pull the image from the remote repository(Docker Hub) and create the container.
    
    ```
    docker run -d \
          -p 2012:3306 \
         --name mysql-docker-container \
         -e MYSQL_ROOT_PASSWORD=root123 \
         -e MYSQL_DATABASE=spring_app_db \
         -e MYSQL_USER=app_user \
         -e MYSQL_PASSWORD=test123 \
            mysql:latest
    ```
    
    After running the above command it will create a docker container with name __“mysql-docker-container“__.
    
    ![Docker run](img/docker.png "Docker Run")
    
3. Building the docker image from project 
    ```
    mvn clean install -DskipTests
    ```
4. Build the docker image with following command :
   ```
   docker build -f Dockerfile -t spring-jpa-app . 
   ``` 
   
   ![Docker Build](img/build.png "Docker Build")
   
5. Running the built docker image 
   
   Now we need to run the built docker image of our spring boot application. Since this application requires to connect with MySQL server, we need to make sure that MySQL server is up and running.
   
   You can check the currently up and running docker containers with following command.
   ```
   docker ps
   ```
   
   ![Docker PS](img/cek.png "Check Docker")
   
6. Link with MySQL Container. 
   
   Once the mysql container is up and running, you can run your spring boot application image on container with following command.  You need to link your spring boot application with mysql container.
   ```
   docker run -t --name spring-jpa-app-container --link mysql-docker-container:mysql -p 8087:8080 spring-jpa-app
   ```
7. Verify containers are linked properly
   
   To verify whether the containers are linked properly, you can get into the application container (**spring-jpa-app-container**) and see the content of the /etc/hosts file.
   
   __login to container with bash mode__
   ```
   docker exec -it spring-jpa-app-container bash
   ```
   ![MySQL Connect](img/mysql_connect.png "MySQL Connect")
   
   ![Add New Data](img/add.png "Add New Data")
   
   ![Check Database MySQL](img/mysql2.png "Check Database MySQL")
   
   ![Check Database MySQL](img/mysql3.png "Check Database MySQL")
   
   